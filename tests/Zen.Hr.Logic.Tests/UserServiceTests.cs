﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;
        Mock<IUserDataAccess> userDataAccess;            

        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            User[] allUsers = concern.GetUsers(false);

            Assert.IsNotNull(allUsers, "The users result cannot be null");
            Assert.AreEqual<int>(3, allUsers.Length, "The user count {0} is incorrect", allUsers.Length.ToString());
            Assert.AreEqual<string>("Diane", allUsers[1].FirstName, "{0} is an invalid user", allUsers[1].FirstName);
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            User[] activeUsers = concern.GetUsers(true);

            Assert.IsNotNull(activeUsers, "The users result cannot be null");
            Assert.AreEqual<int>(2, activeUsers.Length, "The user count {0} is incorrect", activeUsers.Length.ToString());
            Assert.AreEqual<string>("James", activeUsers[1].FirstName, "{0} is an invalid user", activeUsers[1].FirstName);
        }

        [TestInitialize()]
        public void Initialize()
        {
            userDataAccess = new Mock<IUserDataAccess>();

            userDataAccess.Setup(m => m.GetAllUsers())
                .Returns(new User[] {
                    new User{ 
                        FirstName = "Amy", 
                        LastName ="Kruger", 
                        Age = 25, 
                        IsActive = true, 
                        UserName = "akruger"}
                    , new User{
                        FirstName = "Diane",
                        LastName = "Kruger",
                        Age = 26,
                        IsActive = false,
                        UserName = "dkruger"}
                    , new User{
                       FirstName = "James",
                       LastName = "Newton",
                       Age = 45,
                       IsActive = true,
                       UserName = "jNewton"}
                });

            userDataAccess.Setup(m => m.GetAllActiveUsers())
                .Returns(new User[] {
                    new User{ 
                        FirstName = "Amy", 
                        LastName ="Kruger", 
                        Age = 25, 
                        IsActive = true, 
                        UserName = "akruger"}
                    , new User{
                       FirstName = "James",
                       LastName = "Newton",
                       Age = 45,
                       IsActive = true,
                       UserName = "jNewton"}
                });

            concern = new UserService(userDataAccess.Object);
        }
    }
}
