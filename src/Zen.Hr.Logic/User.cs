﻿namespace Zen.Hr.Logic
{
    public class User
    {
        private string userName;
        private string firstName;
        private string lastName;
        private int age;
        private bool isActive;

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public bool IsActive { get; set; }
    }
}